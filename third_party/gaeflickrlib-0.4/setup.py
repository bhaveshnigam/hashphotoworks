from distutils.core import setup
setup(name='gaeflickrlib',
      version='0.3',
      py_modules=['gaeflickrlib'],
      author='Geoffrey Spear',
      author_email='geoffspear@gmail.com',
      classifiers=['Development Status :: 2 - Pre-Alpha',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: BSD License',
                   'Programming Language :: Python :: 2.5',
                   'Topic :: Software Development :: Libraries :: Python Modules'],
      description='Flickr API kit for Google App Engine',
      keywords=['flickr', 'google-app-engine'],
      )
