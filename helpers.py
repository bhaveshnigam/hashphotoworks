import os
import jinja2
import webapp2


TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATE_DIR),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class BaseHandler(webapp2.RequestHandler):

  def render_template(self, template_name, template_values):
    """Renders Jinja2 template."""
    template = JINJA_ENVIRONMENT.get_template(template_name)
    self.response.write(template.render(template_values))
