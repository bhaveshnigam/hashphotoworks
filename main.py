import webapp2
from views import urls


app = webapp2.WSGIApplication(routes=urls.GetRoutes(), debug=True)