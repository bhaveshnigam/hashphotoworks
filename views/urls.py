import webapp2

import views

def GetRoutes():
  return [
      ('/', views.LandingPage),
      ('/generate_upload_url', views.GenerateUploadUrlHandler),
      ('/album/upload', views.CreateAlbum),
      ('/file/([0-9]+)/success', views.AjaxSuccessHandler),
      ('/album/([0-9\w]+)', views.FetchAlbumImages),
      ('/image/([0-9]+)', views.FetchImage),
      ('/import_flickr_images', views.ImportFlickrImages),
      ('/facebook_redirect_uri/?', views.FbRedirectURI),
  ]
