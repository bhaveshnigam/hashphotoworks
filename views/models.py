from google.appengine.ext import ndb

class Album(ndb.Model):
  name = ndb.StringProperty(required=True)
  creation_time = ndb.DateTimeProperty(auto_now_add=True)


class Image(ndb.Model):
  name = ndb.StringProperty(required=True)
  album_id = ndb.IntegerProperty(required=True)
  blob = ndb.BlobKeyProperty()
  uploaded_by = ndb.UserProperty()
  uploaded_at = ndb.DateTimeProperty(required=True, auto_now_add=True)
  is_showcased = ndb.BooleanProperty(default=False)
  image_description = ndb.StringProperty()
  image_url = ndb.StringProperty()

  @classmethod
  def GetShowcased(cls):
    return cls.query(cls.is_showcased==True).order().fetch()

  @classmethod
  def GetByAlbumId(cls, album_id):
    return cls.query(cls.album_id==album_id).order().fetch()


class Users(ndb.Model):
  user = ndb.UserProperty()
  is_admin = ndb.BooleanProperty(default=False)


class FlickrImages(ndb.Model):
  name = ndb.StringProperty(required=True)
  date_added = ndb.DateTimeProperty(required=True, auto_now_add=True)
  image_url = ndb.StringProperty(required=True)
  is_showcased = ndb.BooleanProperty(default=False)
  image_description = ndb.StringProperty()
  flickr_image_id = ndb.StringProperty(required=True)

  @classmethod
  def GetAll(cls):
    return cls.query().order().fetch()

  @classmethod
  def GetShowcased(cls):
    return cls.query(cls.is_showcased==True).order().fetch()

  @classmethod
  def ImageExists(cls, flickr_image_id):
    return bool(cls.query(cls.flickr_image_id == flickr_image_id).fetch())

