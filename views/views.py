import json
import logging
import settings

from google.appengine.api import images
from google.appengine.api import users
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers

import models
from helpers import BaseHandler
from helpers import JINJA_ENVIRONMENT

from third_party import flickr_api
from third_party import flickr


flickr.API_KEY = settings.FLICKR_API_KEY
flickr.API_SECRET = settings.FLICKR_API_SECRET


flickr_api.set_keys(
    api_key=settings.FLICKR_API_KEY, api_secret=settings.FLICKR_API_SECRET)

_FLICKR_ALBUM_ID = 'flickr_bhavesh_photostream'


class LandingPage(BaseHandler):

  def get(self):
    is_admin = False
    album_name = self.request.get('album')
    render_images = []
    image_id = self.request.get('image')
    if image_id.isdigit():
      image_id = int(image_id)
    if image_id:
      requested_image = models.Image.get_by_id(image_id)
      render_images.append({
        'image_name': requested_image.name,
        'image_url': images.get_serving_url(requested_image.blob,
            size=images.IMG_SERVING_SIZES_LIMIT),
        'image_id': str(requested_image.key.id())
      })

    user = users.get_current_user()
    if user:
      if not models.Users.query(models.Users.user == user).fetch():
        new_user = models.Users(
            user=user)
        new_user.put()

    admin_user = models.Users.query(models.Users.user == user).fetch()
    if admin_user:
      admin_user = admin_user[0]
      if admin_user.is_admin:
        is_admin = True
    all_albums = models.Album.query().fetch()
    albums = []

    # Add Showcased to the albums.
    albums.append({'name': 'Showcased', 'id': 'showcased'})

    for album in all_albums:
      albums.append({'name': album.name, 'id': album.key.id()})

    # Add Flickr Photostream to the albums.
    albums.append({'name': 'Flickr Photostream',
                   'id': _FLICKR_ALBUM_ID})

    if album_name.lower() == 'showcased' or not album_name:
      album_name = 'Showcased'
      album_images = models.Image.GetShowcased()
      for image in album_images:
        render_images.append({
          'image_name': image.name,
          'image_url': images.get_serving_url(image.blob,
              size=images.IMG_SERVING_SIZES_LIMIT),
          'image_id': str(image.key.id())
        })
      flickr_images = models.FlickrImages.GetShowcased()
      for image in flickr_images:
        render_images.append({
          'image_name': image.name,
          'image_url': image.image_url,
          'image_id': image.flickr_image_id
        })
    elif album_name.lower() == 'flickr photostream':
        flickr_images = models.FlickrImages.GetAll()
        for image in flickr_images:
          render_images.append(
              {'image_name': image.name,
               'image_url': image.image_url,
               'image_id': image.flickr_image_id})
    else:
      album = models.Album.query(models.Album.name == album_name).fetch()[0]
      album_images = models.Image.query(
         models.Image.album_id == album.key.id()).order().fetch()
      for image in album_images:
        if image.key.id() != image_id:
          render_images.append({
            'image_name': image.name,
            'image_url': images.get_serving_url(image.blob,
                size=images.IMG_SERVING_SIZES_LIMIT),
            'image_id': str(image.key.id())
          })

    self.render_template('home.html',
                         {'albums': albums,
                          'showcased_images': render_images,
                          'album_title': album_name,
                          'is_admin': is_admin,
                          'album_title': album_name})


class CreateAlbum(blobstore_handlers.BlobstoreUploadHandler):

  def post(self):
    album_name = self.request.get('album_name')
    album = None
    if album_name:
      album = models.Album.query(
        models.Album.name == album_name).fetch()
      if album:
        album = album[0]
    if not album:
      album = models.Album(name=album_name)
      album.put()
    blob_info = self.get_uploads()[0]
    image = models.Image(
      name=blob_info.filename,
      album_id=album.key.id(),
      blob=blob_info.key(),
      uploaded_by=users.get_current_user())
    image.put()
    self.redirect("/file/%d/success" % (image.key.id(),))


class AjaxSuccessHandler(BaseHandler):
  def get(self, file_id):
    self.response.headers['Content-Type'] = 'text/plain'
    self.response.out.write('%s/file/%s' % (self.request.host_url, file_id))


class GenerateUploadUrlHandler(BaseHandler):

  def get(self):
    self.response.headers['Content-Type'] = 'text/plain'
    self.response.out.write(blobstore.create_upload_url('/album/upload'))


class FetchAlbumImages(BaseHandler):

  def get(self, album_id):
    is_admin = False
    showcase_images = []
    if album_id.isdigit():
      album_id = int(album_id)
    if album_id == _FLICKR_ALBUM_ID:
        album_name = 'Flickr Photostream'
        flickr_images = models.FlickrImages.GetAll()
        for image in flickr_images:
          showcase_images.append(
              {'image_name': image.name,
               'image_url': image.image_url,
               'image_id': image.flickr_image_id})
    elif str(album_id).lower() == 'showcased':
      album_name = 'Showcased'
      album_images = models.Image.GetShowcased()
      for image in album_images:
        showcase_images.append({
          'image_name': image.name,
          'image_url': images.get_serving_url(image.blob,
              size=images.IMG_SERVING_SIZES_LIMIT),
          'image_id': str(image.key.id())
        })
      flickr_images = models.FlickrImages.GetShowcased()
      for image in flickr_images:
        showcase_images.append({
          'image_name': image.name,
          'image_url': image.image_url,
          'image_id': image.flickr_image_id
        })
    else:
      album = models.Album.get_by_id(album_id)
      album_name = album.name
      album_images = models.Image.GetByAlbumId(album_id)
      for image in album_images:
        showcase_images.append({
          'image_name': image.name,
          'image_url': images.get_serving_url(image.blob,
              size=images.IMG_SERVING_SIZES_LIMIT),
          'image_id': str(image.key.id())
        })

    all_albums = models.Album.query().fetch()
    albums = []

    # Add Showcased to the albums.
    albums.append({'name': 'Showcased', 'id': 'showcased'})

    for album in all_albums:
      albums.append({'name': album.name, 'id': album.key.id()})

    # Add Flickr Photostream to the albums.
    albums.append({'name': 'Flickr Photostream',
                   'id': _FLICKR_ALBUM_ID})

    admin_user = models.Users.query(
        models.Users.user == users.get_current_user()).fetch()
    if admin_user:
      admin_user = admin_user[0]
      if admin_user.is_admin:
        is_admin = True

    content = {'albums': albums,
               'showcased_images': showcase_images,
               'album_title': album_name,
               'is_admin': is_admin}
    self.render_template('album_view.html', content)


class FetchImage(BaseHandler):

  def get(self, image_id):
    if image_id.isdigit():
      image_id = int(image_id)
    image = models.Image.get_by_id(image_id)
    im = images.Image(blob_key=image.blob)
    im.resize(width=1024, height=768)
    transformed_image = im.execute_transforms(
        output_encoding=images.JPEG, quality=40)

    self.response.headers['Content-Type'] = 'image/jpeg'
    self.response.out.write(transformed_image)


class ImportFlickrImages(BaseHandler):

  def get(self):
    flickr_user = flickr_api.Person.findByUserName(
          settings.FLICKR_BHAVESH_USER_NAME)
    public_images = flickr_user.getPublicPhotos()
    for image in public_images:
      image_info = image.getInfo()
      if not models.FlickrImages.ImageExists(image_info.get('id', '')):

        image_url = (settings.FLICKR_IMAGE_SERVING_URL %
            {'farm': image_info.get('farm'), 'server': image_info.get('server'),
             'id': image_info.get('id'), 'secret': image_info.get('secret')})

        img = models.FlickrImages(
            name=image_info.get('title'),
            image_url=image_url,
            flickr_image_id= image_info.get('id'))
        img.put()


class FbRedirectURI(BaseHandler):

  def get(self):
    access_token = self.request.get('access_token')
    expires_in = self.request.get('expires_in')
    self.response.out.write(
        json.dumps({'access_token': access_token,
                    'expires_in': expires_in})
    )

