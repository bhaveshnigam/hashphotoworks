function reAlignHeaderImage() {
      $("#landing-logo").width($(window).width());
      $("#landing-logo").height($(window).height());
      $("#hp-nav-top").height(0.3 * $(window).height());
      $("#showcase-background").width($(window).width());
      $("#showcase-background").height($(window).height());
      $("#background-image").width($(window).width());
      $("#background-image").height($(window).height());

      // Reallign curtains for the resized window.
      $('.curtains').curtain({
         scrollSpeed: 100,
         controls: '.menu',
         curtainLinks: '.curtain-links'
      });

}

function bindEvents() {
  $(window).resize(function() {
    reAlignHeaderImage();
  });
  $('#album-name').blur(function(event) {
    event.target.checkValidity();
  }).bind('invalid', function(event) {
    $('#create-album-btn').prop('disabled', true);
    setTimeout(function() { $(event.target).focus();}, 50);
  });
  $('#myModal').on('hidden', function() {
    $("#create-album-form")[0].reset();
    $('#create-album-btn').prop('disabled', true);
  });

  $('#addPhotos').on('hidden', function() {
    $("#add-photos-form")[0].reset();
    $('#add-photos-btn').prop('disabled', true);
  });
/**
  $("#album-view")[0].onmouseover = function() {
    $("#album-nav-pane").show();
  };
  $("#cover-page")[0].onmouseover = function() {
    $("#album-nav-pane").hide();
 };
*/

  $('.hp-album-name').click(function() {
    var selected_album_name = $(this).text();
    var caret = $('<span>', {class: "caret"});

    $.ajax({
      type: 'GET',
      url: '/album/'+$(this).attr('value')
    })
      .done(function(response) {
        $('#album-view').empty();
        $('#album-view').html(response);
        reAlignHeaderImage();
        bindEvents();
        $(document).ready(function () {
            $("#test-gallery").justifiedGallery();
            $(function() {
              $('#cover-page').css('background-color', 'rgba(0,0,0,0.6)');
            });
            $(function(){
              $('.curtains').curtain({
                 scrollSpeed: 100,
                 controls: '.menu',
                 curtainLinks: '.curtain-links'
              });
            });
          });
        // Parse facebook buttons.
        FB.XFBML.parse();
      });
  });

  $(document).keydown(function(e) {
      if ($('#cover-page').is(':hidden')) {
        if (e.keyCode == 39) {
            $('.gv_panelNavNext').click();
            e.preventDefault();
        }
        if (e.keyCode == 37) {
            $('.gv_panelNavPrev').click();
            e.preventDefault();
        }
     }
  });

   $(document).on('gvPanelUpdated', function(e, eventInfo) {
  //   var updatedImageIndex = eventInfo;
  //   var albumName = $('#album-title').text();
  //   window.history.pushState({}, "",
  //       updateQueryStringParameter(location.href, 'album', albumName));
  //   window.history.pushState({}, "",
  //       updateQueryStringParameter(location.href, 'image', updatedImageIndex));
  });
}


function initiateUploader () {
  uploader = $("#uploader").pluploadQueue({
    runtimes: 'html5,flash',
    use_query_string: false,
    multipart: true,
    flash_swf_url: '/static/plupload/plupload.flash.swf',
    filters : [{title : "Image files", extensions : "jpg,gif,png"}],
    multipart_params : {
    "album_name" : $('#album-name').val()
  },
  preinit: bindSubmitButton,
  init: {
    FileUploaded: function (up, files, info) {
      // destroy the uploader and init a new one
      up.destroy();
      initiateUploader();
    }
  }
  }).pluploadQueue();

  uploader.bind('UploadFile', function(up, file) {
    $.ajax({
        url: '/generate_upload_url',
        async: false,
        success: function(data) {
          up.settings.url = data;
          up.settings.multipart_params.album_name = $('#album-name').val();
        },
    });
  });

  uploader.bind('UploadProgress', function(up, file){
    $('#create-album-btn').prop('disabled', true);
  });

  uploader.bind('FilesAdded', function(up, file) {
    if($('#album-name')[0].validity.valid) {
      $('#create-album-btn').prop('disabled', false);
    }
  });
};

$('#album-name').focusout(function(event) {
  if (uploader.files.length > 0 && $('#album-name')[0].validity.valid) {
    $('#create-album-btn').prop('disabled', false);
  }
});

function bindSubmitButton(uploader) {
  $('#create-album-btn')[0].onclick = function() {
    var album_name = $('#album-name')[0];
    if (album_name.validity.valid) {
      uploader.start();
    }
    else {
      album_name.focus();
    }
  }
}

function scrollbarWidth() {
    var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
        $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
        inner = $inner[0],
        outer = $outer[0];

    jQuery('body').append(outer);
    var width1 = inner.offsetWidth;
    $outer.css('overflow', 'scroll');
    var width2 = outer.clientWidth;
    $outer.remove();
    return (width1 - width2);
}

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?|&])" + key + "=.*?(&|#|$)", "i");
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  } else {
    var hash =  '';
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if( uri.indexOf('#') !== -1 ){
        hash = uri.replace(/.*#/, '#');
        uri = uri.replace(/#.*/, '');
    }
    return uri + separator + key + "=" + value + hash;
  }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function updateButterBar(message) {
  var butterBar = $('#hp-butterbar-message');
  butterBar.text(message.slice(0, 140));
  butterBar.css('display', 'block');
}

function clearButterBar() {
  var butterBar = $('#hp-butterbar-message');
  butterBar.text('');
  butterBar.css('display', 'none');
}
